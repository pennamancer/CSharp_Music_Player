﻿using CSCore;
using CSCore.Codecs;
using CSCore.CoreAudioAPI;
using CSCore.SoundOut;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace Music_Player_Classic_Method
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void playSound()
        {
            using (IWaveSource soundSource = GetSoundSource())
            {
                using (WasapiOut soundOut = new WasapiOut())
                {
                    soundOut.Device = (MMDevice)listBox1.SelectedItem;
                    soundOut.Initialize(soundSource);
                    soundOut.Play();
                    Thread.Sleep(10000); //Controls how long the sound is allowed to play before it's cut. Can probably be set based on length of sound.
                    soundOut.Stop();
                }
            }
        }

        public IWaveSource GetSoundSource()
        {
            return CodecFactory.Instance.GetCodec(@"C:\Library\music\Jojo Part 3 - Stardust Crusaders OST\20. Stardust Crusaders.mp3");
        }

        public List<MMDevice> EnumerateWasapiDevices()
        {
            using (MMDeviceEnumerator enumerator = new MMDeviceEnumerator())
            {
                List<MMDevice> devices = new List<MMDevice>();
                foreach (MMDevice x in enumerator.EnumAudioEndpoints(DataFlow.Render, DeviceState.Active))
                {
                    devices.Add(x);
                }
                return devices;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<MMDevice> deviceList = EnumerateWasapiDevices();
            listBox1.DataSource = deviceList;
            listBox1.DisplayMember = "FriendlyName";
            listBox1.ValueMember = "DeviceID";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            playSound();
        }
    }
}
