﻿using CSCore;
using CSCore.Codecs;
using CSCore.CoreAudioAPI;
using CSCore.SoundOut;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Music_Player
{
    public partial class Form1 : Form
    {
        private IWaveSource Expand;
        private WasapiOut Dong;
        private BindingList<DataStuff> FileList = new BindingList<DataStuff>();

        public Form1()
        {
            InitializeComponent();
        }

        void hook_KeyPressed(object sender, KeyPressedEventArgs args)
        {
            if (Dong != null)
            {
                Dong.Stop();
                CleanupPlayback();
            }

            foreach (DataStuff x in FileList)
            {
                if (x.Hotkey == Convert.ToString(args.Key))
                {
                    Debug.WriteLine(x.SongName + "COCKS");
                    playSound(x.Path);
                }
            }
        }


        public void playSound(string path)
        {
            try //If the user doesn't select a file, catch the resulting exception
            {
                int selectedRowIndex = dataGridView1.SelectedCells[0].RowIndex;
                Expand = CodecFactory.Instance.GetCodec(path);

                Dong = new WasapiOut();
                Dong.Device = (MMDevice)listBox1.SelectedItem;
                Dong.Initialize(Expand);
                Play();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Debug.WriteLine(ex.GetType().ToString());
            }

        }

        public void playSound()
        {
            try //If the user doesn't select a file, catch the resulting exception
            {
                int selectedRowIndex = dataGridView1.SelectedCells[0].RowIndex;
                Expand = CodecFactory.Instance.GetCodec((FileList[selectedRowIndex].Path));

                Dong = new WasapiOut();
                Dong.Device = (MMDevice)listBox1.SelectedItem;
                Dong.Initialize(Expand);
                Play();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Debug.WriteLine(ex.GetType().ToString());
            }

        }

        public void Play()
        {
            Dong.Play();
        }

        public void Stop()
        {
            Dong.Stop();
        }

        private void CleanupPlayback() //Uninitializes the player so the application doesn't throw an exception on exit/new file playback.
        {
            if (Dong != null)
            {
                Dong.Dispose();
                Dong = null;
            }
            if (Expand != null)
            {
                Expand.Dispose();
                Expand = null;
            }
        }

        public List<MMDevice> EnumerateWasapiDevices()
        {
            using (MMDeviceEnumerator enumerator = new MMDeviceEnumerator())
            {
                List<MMDevice> devices = new List<MMDevice>();
                foreach (MMDevice x in enumerator.EnumAudioEndpoints(DataFlow.Render, DeviceState.Active))
                {
                    devices.Add(x);
                }
                return devices;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<MMDevice> deviceList = EnumerateWasapiDevices();
            listBox1.DataSource = deviceList;
            listBox1.DisplayMember = "FriendlyName";
            listBox1.ValueMember = "DeviceID";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Dong != null)
            {
                Dong.Stop();
                CleanupPlayback();
            }
            playSound();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            CleanupPlayback();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Show the dialog and get result.
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                if (FileList.Any(item => item.Path == openFileDialog1.FileName) != true) //Checks if the selected file is already in the list before adding it.
                {
                    FileList.Add(new DataStuff() { Path = openFileDialog1.FileName, SongName = openFileDialog1.SafeFileName, HotKeyHook = new KeyboardHook() });
                }

                //Refreshes the list of files after it has been updated
                dataGridView1.DataSource = FileList;

            }
        }

        public class DataStuff : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            private string path;
            private string songname;
            private string hotkey;
            private KeyboardHook hotkeyhook;

            public string Path
            {
                get { return path; }
                set
                {
                    path = value;
                    NotifyPropertyChanged("Path");
                }
            }

            public string SongName
            {
                get { return songname; }
                set
                {
                    songname = value;
                    NotifyPropertyChanged("SongName");
                }
            }


            public string Hotkey
            {
                get { return hotkey; }
                set
                {
                    hotkey = value;
                    NotifyPropertyChanged("Hotkey");
                }

            }

            public KeyboardHook HotKeyHook
            {
                get { return hotkeyhook; }
                set
                {
                    hotkeyhook = value;
                }
            }

            private void NotifyPropertyChanged(string name)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }

        public class KeyStuff
        {
            public KeyboardHook hook { get; set; }
            public string hookEvent { get; set; }
            public string keyCombo { get; set; }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try { Dong.Stop(); } //If there's no file playing, catches the resulting exception.
            catch (NullReferenceException ex) { }
            CleanupPlayback();
        }

        private void textBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e) //Sets the hotkey for the selected sound if the text box is in focus.
        {
            string keypressed = Convert.ToString(e.KeyCode);
            textBox1.Text = keypressed;
            int selectedRowIndex = dataGridView1.SelectedCells[0].RowIndex;
            FileList[selectedRowIndex].Hotkey = keypressed;

            FileList[selectedRowIndex].HotKeyHook.Dispose();
            FileList[selectedRowIndex].HotKeyHook = new KeyboardHook();
            Keys key = (Keys)Enum.Parse(typeof(Keys), keypressed);
            // register the event that is fired after the key press.
            FileList[selectedRowIndex].HotKeyHook.KeyPressed += new EventHandler<KeyPressedEventArgs>(hook_KeyPressed);
            // registers the key combination to be used as a hotkey.
            FileList[selectedRowIndex].HotKeyHook.RegisterHotKey(Modifierkeys.None, key);
        }
    }
}